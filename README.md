# bydave.net

My personal blogging website. It uses hugo to generate static site. To serve it locally run `hugo server`.

To find the website, visit [bydave.net](https://bydave.net)