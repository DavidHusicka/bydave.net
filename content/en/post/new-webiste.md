---
date: 2023-04-17T21:00:00+02:00
description: "My website just got rebooted"
tags: ["news"]
title: "Website reboot"
---

I've been thinking about updating my website for some time now. Lately, I had to deploy a static website a part of a project for one of my subjects on university. That finally pushed me. So I just created this site and there it is :D. I may not be a great blogger but I hope to learn gradually with experience.

## Goals of this website

As I said, I had plan to do this for a long time. What's the purpose of this site? For most, it will be place I will blog about technological stuff I am working on, mostly open source. I got inspired by the blogs of Mike Blumenkrantz (mine will be just a little bit more serious and not as funny), and fasterthanlime. I am currently busy with my uni but I sure have some plans what to blog about so be patient ;)
